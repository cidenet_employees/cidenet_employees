# Prueba técnica empleados Cidenet

Cidenet requiere de un sistema que le permita tener un control en el registro de sus empleados. Actualmente el proceso se realiza de manera manual sobre una hoja de cálculo, lo cual funciona pero impide alimentar otros procesos para los cuales es importante esta información, así como llevar de manera óptima el registro y administración de la misma.


## Requerimientos

Dado lo anterior, requerimos tu ayuda para tener la mejor solución posible a nuestra necesidad, a continuación listamos las funcionalidades requeridas:

1. Consulta de empleados: Esta pantalla será la pantalla de inicio del sistema. 

    - [x] Se deben listar todos los empleados de Cidenet, hasta 10 por página
    - [x] Se deben presentar los siguientes datos: Primer Nombre, Otros Nombres, Primer Apellido y Correo Electrónico. 
    - [x] Cada registro de empleado tendrá la opción de ser editado, así como también ser eliminado con previa confirmación del sistema 

2. Registro de empleados: El usuario podrá registrar un empleado nuevo.
    - [x] Primer Apellido: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.
    - [x] Primer Nombre: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ. Es requerido y su longitud máxima será de 20 letras.
    - [x] Otros Nombres: Solo permite caracteres de la A a la Z, mayúscula, sin acentos ni Ñ, y el carácter espacio entre nombres. Es opcional y su longitud máxima será de 50 letras.
    - [x] País del empleo: País para el cual el empleado prestará sus servicios, podrá ser Colombia o Estados Unidos. Debe ser una lista desplegable.
    - [x] Correo electrónico: Se debe generar automáticamente, sin intervención del usuario, con el siguiente formato: primer_nombre.primer_apellido.id@dominio(cidenet.com.co para Colombia y cidenet.com.us para Estados Unidos.) su longitud máxima será de 255 caracteres.
    - [x] No podrán existir 2 empleados con el mismo correo electrónico, en caso de que ya exista se debe agregar un valor numérico adicional secuencial (ID). 

3. Edición de empleados: Una vez seleccionada la opción de editar el empleado en la funcionalidad de consulta de empleados, se abrirá la funcionalidad de edición de empleados, la cual tendrá los mismos campos de la funcionalidad de registro de empleados.
    - [x] Permite modificar todos los datos (excepto el correo electrónico).
    - [x]  En caso de que se modifiquen los nombres y/o apellidos, el sistema regenerará su dirección de correo electrónico.


## Instructivo de instalación 

**Tecnologías necesarias**
- Angular CLI
- Node.js
- Visual Studio Code
- Python
- git

Al ingresar al grupo de GitLab *Cidenet_employees* (https://gitlab.com/cidenet_employees) se tiene acceso a los siguientes proyectos que hacen parte del sistema propuesto para la necesidad:

1. Repositorio cidenet_employees_be: Desarrollado en python con Flask y JSON del lado del servidor
    - git clone https://gitlab.com/cidenet_employees/cidenet_employees_be
    - cd 'file-directory'    
    - pip install pip-tools
    - python app.py


2. Repositorio cidenet_employees_fe: Desarrollado en Angular 15
    - git clone https://gitlab.com/cidenet_employees/cidenet_employees
    - cd 'file-directory'
    - npm i
    - ng serve -o


