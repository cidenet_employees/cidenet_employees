import { Component, OnInit, Inject} from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-add-edit-emp',
  templateUrl: './add-edit-emp.component.html',
  styleUrls: ['./add-edit-emp.component.css']
})
export class AddEditEmpComponent implements OnInit{
  empForm: FormGroup;
  paises: string[] = ['Colombia', 'Estados Unidos']
  
  constructor(private fb: FormBuilder, private empService: EmployeeService, public dialogRef: MatDialogRef<AddEditEmpComponent>,  @Inject(MAT_DIALOG_DATA) public data:any) {
    this.empForm = this.fb.group({
      primerApellido: '',
      primerNombre: '',
      otrosNombres: '',
      pais: '',
    })
  }
  ngOnInit(): void {
      this.empForm.patchValue(this.data)
  }
  onFormSubmit(){
    if(this.empForm.valid){
      if(this.data){
        this.empService.editEmployee(this.empForm.value, this.data.id).subscribe({
          next: (res: any) => {
            console.log('onFormSubmitRes',res)
            alert('Detalles actualizados satisfactoriamente');
            this.dialogRef.close(true);
          },
          error: console.log,
        })
      }else{
        this.empService.addEmployee(this.empForm.value).subscribe({
          next: (res: any) => {
            console.log('onFormSubmitRes',res)
            alert('Empleado añadido satisfactoriamente');
            this.dialogRef.close(true);
          },
          error: console.log,
        })
      }
    }
  }
}
