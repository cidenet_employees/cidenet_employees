import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddEditEmpComponent } from './add-edit-emp/add-edit-emp.component';
import { EmployeeService } from './services/employee.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'employers_cidenet';
  displayedColumns: string[] = ['id', 'email', 'primerApellido', 'primerNombre', 'otrosNombres', 'action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private dialog: MatDialog, private empService: EmployeeService){}

  ngOnInit(): void {
    this.getEmployeeJson();    
  }

  openEmpForm(){
    const dialogRef = this.dialog.open(AddEditEmpComponent);
    dialogRef.afterClosed().subscribe({
      next:(res: any) => {
        if(res){
          this.getEmployeeJson();
        }
      },
      error: console.log,
      
    })
  }

  getEmployeeJson() {
    this.empService.getEmployee().subscribe({
      next: (res) =>{
        console.log('getEmployeeJsonRes',res);
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator

      },
      error: console.log,
    })
  }

  deleteEmployee(id:number){
    if(confirm('¿sEstá seguro de que desea eliminar el empleado?')){
      this.empService.deleteEmployee(id).subscribe({
        next: (res) =>{
          alert('Empleado eliminado satisfactoriamente')
          this.getEmployeeJson();
        }
      })
    }
  }

  openEditForm(data: any){
    const dialogRef = this.dialog.open(AddEditEmpComponent,{
      data, 
    });
    dialogRef.afterClosed().subscribe({
      next:(res: any) => {
        if(res){
          this.getEmployeeJson();
        }
      },
      error: console.log,
      
    })
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
