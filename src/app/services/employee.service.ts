import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http: HttpClient) {  }
  
  addEmployee(data: any): Observable<any>{
    return this.http.post('http://localhost:5000/employees', data)
  }

  getEmployee(): Observable<any>{
    return this.http.get('http://localhost:5000/employees')
  }

  deleteEmployee(id: number): Observable<any>{
    return this.http.delete(`http://localhost:5000/employees/${id}`)
  }

  editEmployee(data: any, id:number): Observable<any>{
    return this.http.put(`http://localhost:5000/employees/${id}`, data)
  }
   
  
}
